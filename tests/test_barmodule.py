import pytest

from lemonsqueezer import BarModule, Align

sample_text = 'testytest'


def test_is_abstract():
    with pytest.raises(TypeError):
        BarModule()  # pylint: disable=E0110


class EmptyModule(BarModule):
    async def setup(self, bar):
        return 'setup'

    async def get_output(self):
        return 'output'


@pytest.mark.asyncio
async def test_default_formatting():
    module = EmptyModule()
    assert (await module.get_formatted_output('-', '-', '-')) == 'output'
    assert (await module.formatted_setup(None, '-', '-', '-')) == 'setup'

def test_all_formatting_properties():
    """
    Ensure all properties are added and reset
    """
    module = EmptyModule(padding=1, fg_color='#00F', bg_color='#0F0', u_color='#F00')
    output = module.format_string(sample_text, '#F00', '#00F', '#0F0')

    assert output.index('%{F#00F}') < output.index('%{F#F00}')
    assert output.index('%{B#0F0}') < output.index('%{B#00F}')
    assert output.index('%{U#F00}') < output.index('%{U#0F0}')



import asyncio
from asyncio.subprocess import PIPE
from unittest.mock import MagicMock, call

import pytest
from pytest_mock import MockFixture

import lemonsqueezer as lsq
import lemonsqueezer.modules
from lemonsqueezer.modules import CommandMonitorModule
from lemonsqueezer.modules import ConstantModule
from lemonsqueezer.modules import PeriodicModule

from . import mock_coroutine


class TestPeriodicModule:
    class MockPeriodicModule(PeriodicModule):
        def on_update(self):
            return 'update'

    def test_passes_args(self, mocker):
        mock_init = mocker.patch('lemonsqueezer.BarModule.__init__')
        self.MockPeriodicModule(1, 'these', 'are', 4, keyword='args')
        assert mock_init.call_args == call('these', 'are', 4, keyword='args')

    @pytest.mark.asyncio
    async def test_setup_returns_on_update(self):
        assert (await self.MockPeriodicModule(0).setup(None)) == 'update'

    @pytest.mark.asyncio
    async def test_get_output_waits(self, mocker):
        module = self.MockPeriodicModule(1)

        mock_sleep = mocker.patch('asyncio.sleep', new=mock_coroutine())

        await module.get_output()
        assert mock_sleep.call_args == call(1)

    @pytest.mark.asyncio
    async def test_get_output_returns_on_update(self, mocker):
        module = self.MockPeriodicModule(5)

        mocker.patch('asyncio.sleep', new=mock_coroutine())

        assert (await module.get_output()) == 'update'


class TestCommandMonitorModule:
    example_args = ('command', '--with', 'args')

    class MockCommandMonitorModule(CommandMonitorModule):
        first_output = MagicMock(return_value='first')
        on_line_received = MagicMock(return_value='line_received')

    def test_is_abstract(self):
        class DoesNotImplement(CommandMonitorModule):
            pass

        with pytest.raises(TypeError):
            DoesNotImplement(self.example_args)  # pylint: disable=E0110

    def test_passes_args(self, mocker):
        mock_init = mocker.patch('lemonsqueezer.BarModule.__init__')
        self.MockCommandMonitorModule(1, 'these', 'are', 4, keyword='args')
        assert mock_init.call_args == call('these', 'are', 4, keyword='args')

    @pytest.mark.asyncio
    async def test_setup_returns_first_output(self, mocker):
        module = self.MockCommandMonitorModule(self.example_args)
        mocker.patch('asyncio.create_subprocess_exec', new=mock_coroutine())
        assert (await module.setup(mocker.MagicMock())) == 'first'

    @pytest.mark.asyncio
    async def test_args_passed_to_subprocess(self, mocker):
        subp_exec = mocker.patch('asyncio.create_subprocess_exec', new=mock_coroutine())

        module = self.MockCommandMonitorModule(self.example_args)
        await module.setup(mocker.MagicMock())

        call_args, _ = subp_exec.call_args
        assert call_args == self.example_args

    @pytest.mark.asyncio
    async def test_get_output_exception_on_process_killed(self):
        module = self.MockCommandMonitorModule(self.example_args)
        module._process = await asyncio.create_subprocess_exec(
            'tail', '-f', '/dev/null', stdout=PIPE)
        module._process.kill()
        await module._process.wait()

        with pytest.raises(BrokenPipeError):
            await module.get_output()

class TestConstantModule:
    example_text = 'textytext'

    def test_passes_args(self, mocker: MockFixture):
        mock_init = mocker.patch('lemonsqueezer.BarModule.__init__')

        ConstantModule(self.example_text, 'these', 'are', 4, keyword='args')
        assert mock_init.call_args == call('these', 'are', 4, keyword='args')

    def test_setup_returns_immediately(self, event_loop: asyncio.AbstractEventLoop):
        module = ConstantModule(self.example_text)
        event_loop.run_until_complete(asyncio.wait_for(module.setup(MagicMock()), 0.2))

    @pytest.mark.asyncio
    async def test_get_output_runs_forever(self):
        module = ConstantModule(self.example_text)
        await module.setup(MagicMock())

        with pytest.raises(asyncio.TimeoutError):
            await asyncio.wait_for(module.get_output(), 0.2)

from lemonsqueezer import backgrounded, colored, underlined

input_text = 'testytest'


class TestBackround:
    def test_beginning(self):
        assert backgrounded(input_text, '#F00')[:8] == '%{B#F00}'

    def test_reset(self):
        assert backgrounded(input_text, reset_color='#F00')[-8:] == '%{B#F00}'

    def test_includes_input(self):
        assert input_text in backgrounded(input_text)


class TestForeground:
    def test_beginning(self):
        assert colored(input_text, '#F00')[:8] == '%{F#F00}'

    def test_reset(self):
        assert colored(input_text, reset_color='#F00')[-8:] == '%{F#F00}'

    def test_includes_input(self):
        assert input_text in colored(input_text)


class TestUnderline:
    def test_beginning(self):
        assert underlined(input_text, '#F00')[:8] == '%{U#F00}'

    def test_reset(self):
        assert underlined(input_text, reset_color='#F00')[-8:] == '%{U#F00}'

    def test_enables(self):
        assert underlined(input_text, color='#FFF', reset_color='#000')[8:13] == '%{+u}'

    def test_disables(self):
        assert underlined(input_text, color='#FFF', reset_color='#000')[-13:-8] == '%{-u}'

    def test_includes_input(self):
        assert input_text in underlined(input_text)

"""
A set of premade, parameterized modules to cover some of the most common
usecases for the lemonbar
"""

from abc import abstractmethod, ABCMeta
from asyncio.subprocess import PIPE
import asyncio
import string
import subprocess
from typing import List
import re

import lemonsqueezer as lsq

import logging


def _get_cmd_output(*args, **kwargs):
    """
    get a command's output using `subprocess.run`. `args` specify the command to be run,
    while `kwargs` get passed directly to subprocess.run
    """
    return subprocess.run(args, stdout=PIPE, **kwargs).stdout.decode()


class ConstantModule(lsq.BarModule):
    """ 
    Show a constant string on the bar. Can be used for simple buttons
    """
    def __init__(self, text, *args, _loop=None, **kwargs):
        """
        :param text: The string to show on the bar
        """
        super().__init__(*args, **kwargs)
        self.text = text

        self._lock = asyncio.Lock(loop=_loop)

    # show it once...
    async def setup(self, bar):
        await self._lock.acquire()
        return self.text

    # ...and the wait forever
    async def get_output(self):
        await self._lock.acquire()


class PeriodicModule(lsq.BarModule, metaclass=ABCMeta):
    """
    Show text on the bar that updates every few seconds. This is the way most
    other bars/bar managers work. Before using this, see if
    :class:`~lemonsqueezer.modules.CommandMonitorModule` can be used instead, as
    it can be much more efficient
    """
    def __init__(self, interval, *args, **kwargs):
        """
        :param interval: How often to update the module
        """
        super().__init__(*args, **kwargs)
        self.interval = interval

    async def setup(self, bar):
        return self.on_update()

    async def get_output(self):
        await asyncio.sleep(self.interval)
        return self.on_update()

    @abstractmethod
    def on_update(self):
        """
        This is run on every update.

        Return the new text to be displayed on the bar, or None, if no update
        should be made
        """
        pass


class CommandMonitorModule(lsq.BarModule, metaclass=ABCMeta):
    """
    A module that updates whenever a command produces a line of output. This can
    replace most modules that would update periodically to show system
    information, while being much more efficient as the bar is only written to
    when a change happens
    """
    def __init__(self, command_args: List[str], *args, **kwargs):
        """
        :param command_args: The command to be run as a list of strings
        """
        super().__init__(*args, **kwargs)
        self.command_args = command_args
        self._process = None

    async def setup(self, bar):
        self._process = await asyncio.create_subprocess_exec(
            *self.command_args, stdout=PIPE)
        return self.first_output(bar)

    async def get_output(self):
        if self._process.returncode is not None:
            raise BrokenPipeError
        process_output =\
            (await self._process.stdout.readline()).decode().strip()
        return self.on_line_received(process_output)

    @abstractmethod
    def first_output(self, bar):
        """
        Run immediately when the bar is started. Receives the bar instance as an
        argument. The return value is set as the text of the module.
        """
        return '-'

    @abstractmethod
    def on_line_received(self, process_output):
        """
        Run whenever the command outputs a full line of output. 
        Receives that output as an argument. The return value of this function
        is shown on the module.
        """
        return ''


class TickTock(PeriodicModule):
    """
    Test module. Switches between tick and tock every time an interval passes
    """
    def __init__(self, interval, *args, **kwargs):
        """
        :param interval: How often to switch between "tick" and "tock"
        """
        super().__init__(interval, *args, **kwargs)
        self.tick = False

    def on_update(self):
        self.tick = not self.tick
        return 'tick' if self.tick else 'tock'


class Clock(PeriodicModule):
    """
    A clock based on the 'date' command
    """
    def __init__(self, format_str, interval, icon='', *args, **kwargs):
        """
        :param format_str: The format string to pass to the 'date' command
        :param interval: How often to update the call
        """
        super().__init__(interval, *args, **kwargs)
        self.format_str = format_str
        self.icon = icon

    def on_update(self):
        out = _get_cmd_output('date', self.format_str).strip()
        return f'{self.icon} {out}'


class Counter(lsq.BarModule):
    """
    Test module: A counter that increases, decreases or resets 
    """
    async def setup(self, bar):
        self.count = 0
        self.inc_lock = asyncio.Lock()
        await self.inc_lock.acquire()

        def inc():
            self.count += 1
            self.inc_lock.release()

        def dec():
            self.count -= 1
            self.inc_lock.release()

        def reset():
            self.count = 0
            self.inc_lock.release()

        self.button = lsq.Button(
            str(self.count), (1, inc), (2, reset), (3, dec))
        bar.register_button(self.button)

        return self.button

    async def get_output(self):
        await self.inc_lock.acquire()
        self.button.text = str(self.count)
        return self.button


LemonSqueezer
=============

**LemonSqueezer** is a python wrapper for lemonbar that makes it easy to make
beautiful, informative status bars using *less power* than most other status
bars. This is achieved by only updating the bar when new output is available,
without polling commands for output, (such as polybar or a custom lemonbar
script would do).

Installation
------------

- Arch Linux

  ```sh
  git clone https://aur.archlinux.org/python-lemonsqueezer.git
  cd python-lemonsqueezer
  makepkg -si
  ```

- General

  ```sh
  sudo pip install python-lemonsqueezer
  ```

Basics
------

Lemonsqueezer defines its output in terms of `Modules`. These are python
objects which define how and when to render output on the bar. The base
`BarModule` class has little to no behaviour attached to it and so, to create
useful modules, a couple of helper subclasses are available (See
`CommandWatcherModule` or `PeriodicModule` in the docs). There are also a few
premade modules for things such as battery or a clock.

Creating custom modules
-----------------------

New modules are created by subclassing the base `BarModule` class and
overriding the `setup` and `get_output` methods.

Lemonsqueezer uses asyncio to achieve its efficiency. Both `setup` and
`get_output` are coroutine methods. When the bar starts, `setup` is awaited
first. When it returns, its output is placed on the bar at the appropriate
location (as specified by the BarModule constructor). After that,
`get_output` is awaited repeatedly, setting the module text whenever it
returns. Note that if `get_output` returns None, then the bar is not updated
at all.

Helper subclasses
-----------------

- `PeriodicModule`

  Run a command every few seconds and set the output based on its output

  This is the closest to how other bars usually get output, and it's probably
  the most inefficient method to create a module. However you can still have
  a small improvement over a traditional bar manager since you are able to
  return None if there are no changes to the output.

- `CommandMonitorModule`

  Use this type of module when you want to have the module update whenever a
  command produces some output. A lot of the built-in modules are defined using this.
API Reference
=====================

lemonsqueezer
-------------

.. automodule:: lemonsqueezer
    :members:
    :show-inheritance:

lemonsqueezer.modules
---------------------
.. automodule:: lemonsqueezer.modules

.. autoclass:: lemonsqueezer.modules.ConstantModule

.. autoclass:: lemonsqueezer.modules.PeriodicModule
    :members: on_update

.. autoclass:: lemonsqueezer.modules.CommandMonitorModule
    :members: first_output, on_line_received

.. autoclass:: lemonsqueezer.modules.Clock
.. autoclass:: lemonsqueezer.modules.BSPWMDesktops
.. autoclass:: lemonsqueezer.modules.Battery
.. autoclass:: lemonsqueezer.modules.MediaControls

